package com.mulaczos.fileorganizer.dto;

import java.io.File;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import lombok.Data;

@Data
public class Item {

    private String fullPathToFile;
    private String pathToDirectory;
    private String filename;
    private boolean dir;
    private String extension;
    private LocalDateTime creationDate;
    private LocalDateTime modifyDate;

    public Item(String fullPathToFile, FileTime creationTime, FileTime lastModifiedTime, boolean directory, String extension) {
        this.fullPathToFile = fullPathToFile;
        this.pathToDirectory = this.fullPathToFile.substring(0, this.fullPathToFile.lastIndexOf(File.separator));
        this.filename = this.fullPathToFile.substring(this.fullPathToFile.lastIndexOf(File.separator));
        this.creationDate = LocalDateTime.ofInstant(creationTime.toInstant(), ZoneOffset.UTC);
        this.modifyDate = LocalDateTime.ofInstant(lastModifiedTime.toInstant(), ZoneOffset.UTC);
        this.dir = directory;
        this.extension = extension;
    }
}
