package com.mulaczos.fileorganizer.dto;

import java.util.Arrays;
import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@RequiredArgsConstructor
public enum FileExtension {

    AUDIO(Arrays.asList("ac3", "flac", "mp3", "m2t", "m4a", "wav", "wma", "wav"), PathResolvePolicy.AT_THE_BEGINNING),
    PAINTINGS(Arrays.asList("bmp", "png"), PathResolvePolicy.AT_THE_BEGINNING),
    RECORDINGS(Arrays.asList("amr"), PathResolvePolicy.AT_THE_BEGINNING),
    DOCUMENTS(Arrays.asList("doc", "rtf", "pdf", "txt"), PathResolvePolicy.AT_THE_BEGINNING),
    MULTIMEDIA(Arrays.asList("3gp", "avi", "mov", "mp4", "mpeg", "mpg", "mts", "wmv", "jpeg", "jpg"), PathResolvePolicy.AT_THE_END),
    OTHER(Arrays.asList(" "), PathResolvePolicy.ONE_LOCATION);

    @Getter(AccessLevel.PRIVATE)
    private final List<String> extensions;
    private final PathResolvePolicy policy;

    public static FileExtension getCorrespondingFileExtension(String extension) {
        return Arrays.stream(values())
                .filter(fileExtensions -> fileExtensions.containsExtension(extension))
                .findAny()
                .orElse(OTHER);
    }

    private boolean containsExtension(String extension) {
        if (extension == null) {
            return false;
        }
        return this.extensions.stream().anyMatch(v -> extension.toLowerCase().contains(v));
    }

    @Override
    public String toString() {
        return "FileExtension{" +
                "name=" + name() +
                ", policy=" + policy +
                '}';
    }
}
