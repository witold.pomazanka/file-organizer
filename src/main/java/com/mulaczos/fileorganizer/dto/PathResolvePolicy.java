package com.mulaczos.fileorganizer.dto;

public enum PathResolvePolicy {
    AT_THE_BEGINNING, AT_THE_END, ONE_LOCATION
}
