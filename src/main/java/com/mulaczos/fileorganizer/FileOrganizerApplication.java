package com.mulaczos.fileorganizer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class FileOrganizerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileOrganizerApplication.class, args);
    }
}
