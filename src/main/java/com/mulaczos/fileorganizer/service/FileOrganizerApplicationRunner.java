package com.mulaczos.fileorganizer.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;

@Slf4j
@Component
public class FileOrganizerApplicationRunner implements ApplicationRunner {

    @Autowired
    private FileOrganizerService fileOrganizer;

    @Override
    public void run(ApplicationArguments args) {
        log.info("File organizer execution started");
        LocalDateTime startDateTime = LocalDateTime.now();
        fileOrganizer.organize();
        log.info("File organizer has finished");
        log.info("Operation took: " + Duration.between(startDateTime, LocalDateTime.now()).toString());
    }


}
