package com.mulaczos.fileorganizer.service;

import com.mulaczos.fileorganizer.dto.FileExtension;
import com.mulaczos.fileorganizer.dto.Item;
import com.mulaczos.fileorganizer.dto.PathResolvePolicy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Slf4j
@Service
public class FileOrganizerService {

    private final String UNKNOWN = "UNKNOWN";
    @Value("${file.organizer.start.location.path}")
    private String startLocation;
    @Value("${file.organizer.destination.path}")
    private String destinationFolderName;
    @Value("#{new Boolean('${file.organizer.should.delete.file.after.copying}')}")
    private boolean shouldDeleteFilesAfterCopying;

    public void organize() {
        log.info("Start location {}", startLocation);
        log.info("Destination location {}", destinationFolderName);
        var data = gatherData(this.startLocation);
        log.info("\n\n=== ITEMS FOUND: {}", data.size());
        moveFiles(data);
    }

    public Set<Item> gatherData(String startLocation) {
        log.debug("Getting data for item: {}", startLocation);
        var preparedItems = getFolderPathsForLocation(startLocation);
        var allLocations = new HashSet<>(preparedItems);
        for (Item item : preparedItems) {
            allLocations.addAll(gatherData(item.getFullPathToFile()));
        }
        return allLocations;
    }

    private Set<Item> getFolderPathsForLocation(String location) {
        log.debug("Getting files and dirs for location: {}", location);
        var dirsAndFiles = new File(location).list();
        if (isNull(dirsAndFiles)) {
            return Collections.emptySet();
        }
        return prepareItems(location, dirsAndFiles);
    }

    private void moveFiles(Set<Item> data) {
        log.info("Moving all files to new locations");
        for (Item item : data) {
            if (item.isDir()) {
                log.debug("Skipped moving directory: {}", item.getFullPathToFile());
                continue;
            }
            var desiredPath = createFolderIfNotExists(item);
            log.debug("Directory to copy file to: {}", desiredPath);
            var source = Paths.get(item.getFullPathToFile());
            var target = Paths.get(desiredPath + File.separator + item.getFilename());
            try {
                log.debug("Copying file: {}, to: {}", source, target);
                Files.copy(source, target);
                log.debug("Successfully copied file from source: {} to target {}", source, target);
                boolean successfulDeletion = deleteFile(source, shouldDeleteFilesAfterCopying);
                if (!successfulDeletion) {
                    log.warn("File has not been deleted {}", source);
                }
            } catch (FileAlreadyExistsException e) {
                log.warn("File already exists in location, source {}, target {}", source, target);
            } catch (IOException e) {
                log.error("Exception copying or deleting file", e);
            }
        }
    }

    private boolean deleteFile(Path source, boolean shouldDeleteFile) {
        if (!shouldDeleteFile) {
            log.debug("Omitted deleting a file: {}", source);
            return true;
        }
        log.debug("Deleted file: {}", source);
        return source.toFile().delete();
    }

    private String createFolderIfNotExists(Item item) {
        log.debug("Creating folder if not exists: {}", destinationFolderName);
        var fileExtension = FileExtension.getCorrespondingFileExtension(item.getExtension());
        log.debug("Resolved folder name: {} for file extension: {}", fileExtension.name(), item.getExtension());
        var desiredPath = prepareAndResolvePath(destinationFolderName, item, fileExtension);
        log.debug("Desired path: {}", desiredPath);
        var tmp = new File(desiredPath);
        if (!tmp.exists()) {
            log.debug("Directory not exists, creating..");
            try {
                return Files.createDirectories(Paths.get(desiredPath)).toString();
            } catch (FileAlreadyExistsException e) {
                log.error("Caught dir already exists exception, for: {}", desiredPath, e);
                return desiredPath;
            } catch (IOException e) {
                log.error("Exception creating directory", e);
            }
        }
        return tmp.getAbsolutePath();
    }

    private String prepareAndResolvePath(String destinationRootLocation, Item item, FileExtension fileExtension) {
        log.debug("Preparing path for root location: {} item: {} and file extension: {}", destinationRootLocation, item, fileExtension);
        var modifyDate = item.getModifyDate();
        var creationDate = item.getCreationDate();
        String montName;
        int year;
        int month;
        if (nonNull(modifyDate)) {
            year = modifyDate.getYear();
            month = modifyDate.getMonth().getValue();
        } else if (nonNull(creationDate)) {
            log.info("Modification date was null, taking creation date");
            year = creationDate.getYear();
            month = creationDate.getMonth().getValue();
        } else {
            log.info("Both create and modify date was null");
            month = 999;
            year = 999;
        }
        var beginning = destinationRootLocation + File.separator;
        return resolvePath(item, fileExtension, year, month, beginning);
    }

    private String resolvePath(Item item, FileExtension fileExtension, int modifyYear, int monthNumber, String beginning) {
        log.debug("Resolving path for beginning path: {}, item: {}, file extension: {}, modify year: {}",
                beginning, item, fileExtension, modifyYear);
        if (fileExtension.getPolicy().equals(PathResolvePolicy.AT_THE_END)) {
            return beginning + modifyYear + File.separator + modifyYear + "." + String.format("%02d", monthNumber) + File.separator;
        } else if (fileExtension.getPolicy().equals(PathResolvePolicy.AT_THE_BEGINNING)) {
            return beginning + fileExtension.name() + File.separator + modifyYear + File.separator;
        } else {
            return beginning + fileExtension.name() + File.separator + modifyYear + File.separator + item.getExtension().toUpperCase() + File.separator;
        }
    }

    private String determineFileExtension(String name) {
        int i = name.lastIndexOf('.');
        if (i > 0) {
            return name.substring(i + 1);
        }
        return UNKNOWN;
    }

    private Set<Item> prepareItems(String location, String[] dirsAndFiles) {
        Set<Item> items = new HashSet<>();
        log.debug("Preparing items objects for given dirs and files");
        Arrays.stream(dirsAndFiles).forEach(i -> {
            try {
                var path = location + File.separator + i;
                log.debug("Resolving attrs for given item: {}", path);
                var attr = Files.readAttributes(Paths.get(path), BasicFileAttributes.class);
                var singleItem = new Item(path, attr.creationTime(), attr.lastModifiedTime(), attr.isDirectory(), determineFileExtension(path));
                log.debug("Created item with properties: {}", singleItem);
                items.add(singleItem);
            } catch (IOException e) {
                log.error("Problem with reading file ", e);
            }
        });
        return items;
    }
}
