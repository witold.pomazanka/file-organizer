# file-organizer

## Requirements

For building and running the application you need:

- [JDK 11](https://www.oracle.com/pl/java/technologies/javase/jdk11-archive-downloads.html)
- [Maven 3](https://maven.apache.org)